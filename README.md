# devops-pair4

Group project work for the course Continuous Development and Deployment (COMP.SE.140)

# Instructions for deploying automatically to EC2 via Gitlab CI

Configure CI environment variables:

- SSH_PRIVATE_KEY: Private key to access the ec2 instance.
- EC2_INSTANCE: connection url for ssh connection (eg. "ubuntu@22.22.22.22").
- SSH_FINGERPRINT: Fingerprint of SSH key to verify that the EC2 host is correct one.

Gitlab CI will set up the environment for connecting to EC2 using before_script in deploy job.
Then ssh connection is made to the instance and we fetch the correct commit (using Gitlab global variable CI_COMMIT_SHA) and
run docker-compose up -d to start the containers.

The live environment is available at: http://ec2-13-53-46-69.eu-north-1.compute.amazonaws.com:8081/

# Instructions to test the system

## Run the system

```bash
# cloning the repository
git clone https://gitlab.com/nasong/devops-pair4.git

# build the system through docker-compose
cd devops-pair4
docker-compose build --no-cache

# run the system
docker-compose run -d
```

## Run test scripts

```bash
# build a container for test
docker build . -t gateway_test --no-cache

# run the test script
docker run --network devops-pair4_gateway_network gateway_test
```

## Test from terminal

```bash
# GET /messages
curl http://localhost:8081/messages

# GET /state
curl http://localhost:8081/state

# PUT /state
curl --request PUT --url http://localhost:8081/state --header 'content-type: application/x-www-form-urlencoded' --data 'state=INIT' 

curl --request PUT --url http://localhost:8081/state --header 'content-type: application/x-www-form-urlencoded' --data 'state=PAUSED' 

curl --request PUT --url http://localhost:8081/state --header 'content-type: application/x-www-form-urlencoded' --data 'state=RUNNING' 

curl --request PUT --url http://localhost:8081/state --header 'content-type: application/x-www-form-urlencoded' --data 'state=SHUTDOWN' 

# GET /run-log
curl http://localhost:8081/run-log
```

Test for the live application from the cloud is available by replacing the server url from http://localhost:8081 to http://ec2-13-53-46-69.eu-north-1.compute.amazonaws.com:8081
