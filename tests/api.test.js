let chai = require("chai");
let chaiHttp = require("chai-http");
let server = process.env.APIGATEWAY_URL || "http://localhost:8081"; //require('../apiGateway/apiGateway');
let should = chai.should();

chai.use(chaiHttp);

console.log(server);
let api = chai.request(server);

function timeout(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

describe("GET /state", () => {
  it("get the value of state", async () => {
    const res = await api.get("/state");
    res.should.have.status(200);
    res.should.haveOwnProperty("text");
    res.text.should.be
      .a("string")
      .oneOf(["INIT", "PAUSED", "RUNNING", "SHUTDOWN", ""]);
  });
});

describe("PUT /state", () => {
  it("updates the current state to INIT", async () => {
    let res = await api
      .put("/state")
      .set("content-type", "application/x-www-form-urlencoded")
      .send({ state: "INIT" });
    res.should.have.status(200);
    res = await api.get("/state");
    res.should.have.status(200);
    res.should.haveOwnProperty("text");
    res.text.should.be.a("string").equal("INIT");
  });
  it("updates the current state to PAUSED", async () => {
    let res = await api
      .put("/state")
      .set("content-type", "application/x-www-form-urlencoded")
      .send({ state: "PAUSED" });
    res.should.have.status(200);

    res = await api.get("/state");
    res.should.have.status(200);
    res.should.haveOwnProperty("text");
    res.text.should.be.a("string").equal("PAUSED");
  });
  it("updates the current state to RUNNING", async () => {
    let res = await api
      .put("/state")
      .set("content-type", "application/x-www-form-urlencoded")
      .send({ state: "RUNNING" });
    res.should.have.status(200);

    res = await api.get("/state");
    res.should.have.status(200);
    res.should.haveOwnProperty("text");
    res.text.should.be.a("string").equal("RUNNING");
  });
  it("updates the current state to SHUTDOWN", async () => {
    let res = await api
      .put("/state")
      .set("content-type", "application/x-www-form-urlencoded")
      .send({ state: "SHUTDOWN" });
    res.should.have.status(200);

    res = await api.get("/state");
    res.should.have.status(200);
    res.should.haveOwnProperty("text");
    res.text.should.be.a("string").equal("SHUTDOWN");
  });
});

describe("GET /messages", () => {
  it("Returns all message registered with OBSE-service", async () => {
    let res = await api
      .put("/state")
      .set("content-type", "application/x-www-form-urlencoded")
      .send({ state: "RUNNING" });

    res.should.have.status(200);

    // verify state has been set to RUNNING
    res = await api.get("/state");
    res.should.have.status(200);
    res.should.haveOwnProperty("text");
    res.text.should.be.a("string").equal("RUNNING");

    // Wait until get enough messages
    await timeout(15000);
    res = await api.get("/messages");
    res.should.have.status(200);
    res.should.haveOwnProperty("text");
    res.text.should.be.a("string");

    // check correct print format
    const lines = res.text.split("\n");
    for (const line of lines) {
      if (line !== "")
        line.should.to.match(
          /\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d\.\d+Z Topic (my.o|my.i):"(\d|\D)*"/
        );
    }
  });
});

describe("GET /run-log", () => {
  it("Get information about state changes", async () => {
    const res = await api.get("/run-log");
    res.should.have.status(200);
    res.should.haveOwnProperty("text");
    res.text.should.be.a("string");

    const lines = res.text.split("\n");

    for (const line of lines) {
      if (line !== "")
        line.should.to.match(
          /\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d\.\d+Z: (INIT|PAUSED|RUNNING|SHUTDOWN)/
        );
    }
  });
});
