var express = require('express');
var bodyParser = require('body-parser');
var proxy = require('express-http-proxy')
var url = require('url')
var http = require('http')
// set initial state
var app = express();
var fs = require('fs');

var state = 'INIT';
const filePath = '/gateway/run-log.txt'
app.use(bodyParser.urlencoded({ extended: false }))

// GET /messages
app.use('/messages', proxy('httpserver:8080'), function (req, res) {
    // Set state to SHUTDOWN
    req.setTimeout(5000, function () {
        console.log("HTTPSERVER is not listening. No messages to return");
        req.abort();
    })
});

// PUT /state
app.put('/state', function (req, res) {
    console.log("payload type:", req.body.state);

    const postData = JSON.stringify(req.body);
    const options = {
        hostname: 'original',
        port: 8002,
        path: '/state',
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
            'Content-length': Buffer.byteLength(postData)
        }
    }

    const post_req = http.request(options, (post_res) => {
        console.log(`STATUS: ${post_res.statusCode}`);
        console.log(`HEADERS: ${JSON.stringify(post_res.headers)}`);
        post_res.setEncoding('utf8');
        post_res.on('data', (chunk) => {
            console.log(`BODY: ${chunk}`);
        });
        post_res.on('end', () => {
            console.log('No more data in response.');
        });
    });

    post_req.write(postData);
    post_req.on('error', (e) => {
        console.error(`problem with request: ${e.message}`);
        state = 'SHUTDOWN';
    });

    post_req.end();
    res.end();

    // write runlog whenever state changes
    var date = new Date();
    var timestamp = date.toISOString();
    var row = `${timestamp}: ${req.body.state} \n`;

    // write state changes to log
    fs.writeFileSync(filePath, row, { flag: "a+" }, function (err) {
        if (err) {
            // append failed
            console.log("error occured in writing file", err)
        } else {
            console.log("line added to the file in docker volume")
        }
    })

    state = req.body.state
    
})

// GET /state
app.get('/state', function(req,res) {
    res.send(state);
    res.end();
})

// GET /run-log
app.get('/run-log', function(req, res) {
    fs.access(filePath, fs.constants.F_OK, function (err) {
        if (err) {
            res.writeHead(400, { 'Content-Type': 'text/plain' });
            res.end("ERROR! File does not exist");
        }
        else {
            var stat = fs.statSync(filePath);

            res.writeHead(200, {
                'Content-Type': 'text/plain',
                'Content-Length': stat.size
            });

            let readStream = fs.createReadStream(filePath);
            readStream.pipe(res);
        }
    })
})

app.listen(8081);

// curl command for sending PUT request
// curl --request PUT --url http://localhost:8081/state --header 'content-type: application/x-www-form-urlencoded' --data 'state=INIT'

// ENV DEBUG=express:*