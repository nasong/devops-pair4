FROM node:14.8.0

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install

COPY . .

ENV APIGATEWAY_URL="apiGateway:8081"
CMD ["npm","test"]