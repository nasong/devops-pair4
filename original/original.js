// import { exec } from 'child_process'
var fs = require('fs');
var childProcess = require('child_process');
var sendTask = require('./rabbit-utils/sendTask.js');
const host = 'rabbitmq';
var express = require('express');
var bodyParser = require('body-parser');
const { allowedNodeEnvironmentFlags } = require('process');
const headers = { 'x-delay': 3000 };
var app = express();
app.use(bodyParser.json());

function sendMessage() {
    for (i = 1; i < 4; i++) {
        msg = `MSG_${i}`
        sendTask.addTask(host, "my.o", msg, { headers })
    }
}

var timeout = undefined

const ORIG = async function ORIG_func() {
    timeout = setInterval(sendMessage, 5000)
}

app.put('/state', function(req, res) {
    if ( req.body.state === 'INIT' || req.body.state === 'RUNNING') {
        if (!timeout) {
            ORIG();
        }
    }
    else if ( req.body.state  === 'PAUSED' || req.body.state === 'SHUTDOWN') {
        if (timeout) {
            clearInterval(timeout)
            timeout = undefined
        }
    }
    else {
        console.log("wrong state type! States can be only INIT, RUNNING, PAUSED or SHUTDOWN.")
    }
})
    
app.listen(8002, function (req, res) {
    console.log("ORIG service listening on port 8002. PUT state 'INIT' to start")
})